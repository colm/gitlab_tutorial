// stdlib functionality
#include <iostream>
// ROOT functionality
#include <TFile.h>
#include <TH1D.h>
#include <TCanvas.h>
#include <TEnv.h>
// ATLAS EDM functionality
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODMuon/MuonContainer.h"
#include <AsgTools/MessageCheck.h>

using namespace std;

int main() {

  using namespace asg::msgUserCode;
  ANA_CHECK_SET_TYPE (int);
  // initialize the xAOD EDM
  ANA_CHECK(xAOD::Init());
  
  //Read config file using TEnv
  TEnv env("env");
  env.ReadFile("/home/atlas/Bootcamp/source/analysis.config", EEnvLevel(0));
  double j_pt_cut = stod(env.GetValue("minjetpt",""));
  double j_eta_cut = stod(env.GetValue("maxjeteta",""));

  cout<<"====Configuration==="<<endl;
  cout<<"Reading data file: "<< env.GetValue("inputfile","")<<endl;
  cout<<"Pt cut: "<<j_pt_cut<<endl;
  cout<<"Eta cut: "<<j_eta_cut<<endl;
  // open the input file
  TString inputFilePath = env.GetValue("inputfile","");
  xAOD::TEvent event;
  std::unique_ptr< TFile > iFile ( TFile::Open(inputFilePath, "READ") );
  ANA_CHECK(event.readFrom( iFile.get() ));

  // for counting events
  unsigned count = 0;

  // get the number of events in the file to loop over
  const Long64_t numEntries = event.getEntries();

  //Make histograms
  TH1D h_n_jets("h_n_jets","Number of Jets;N Jets Per Event;N Events/Bin",20,0,20);
  TH1D h_mjj("h_mjj","Dijet Invariant Mass;Dijet Invariant Mass [GeV];N Events/Bin",100,0,500);

  TLorentzVector jet1, jet2, jj;
  // primary event loop
  for ( Long64_t i=0; i<numEntries; ++i ) {

    // Load the event
    event.getEntry( i );

    // Load xAOD::EventInfo and print the event info
    const xAOD::EventInfo * ei = nullptr;
    ANA_CHECK (event.retrieve( ei, "EventInfo" ));
    //std::cout << "Processing run # " << ei->runNumber() << ", event # " << ei->eventNumber() << std::endl;

    // retrieve the jet container from the event store
    const xAOD::JetContainer* jets = nullptr;
    const xAOD::ElectronContainer* electrons = nullptr;
    const xAOD::MuonContainer* muons = nullptr;
    ANA_CHECK(event.retrieve(jets, "AntiKt4EMTopoJets"));
    ANA_CHECK(event.retrieve(electrons, "Electrons"));
    ANA_CHECK(event.retrieve(muons, "Muons"));

    //print number of muons and electrons in each event
    cout<<"N Electrons: "<< electrons->size()<<endl;
    cout<<"N Muons: "<< muons->size()<<endl;
    jet1.SetPtEtaPhiM(0,0,0,0);
    jet2.SetPtEtaPhiM(0,0,0,0);
    // loop through all of the jets and make selections with the helper
    for(const xAOD::Jet* jet : *jets) {
      // print the kinematics of each jet in the event
      //std::cout << "Jet : pt=" << jet->pt() << "  eta=" << jet->eta() << "  phi=" << jet->phi() << "  m=" << jet->m() << std::endl;
      // Find leading jet
      if ((jet->pt()/1000 < j_pt_cut) || (jet->eta() > j_eta_cut)){
        continue;
      }
      if (jet->pt()/1000 > jet1.Pt()){
        jet2 = jet1;
        jet1.SetPtEtaPhiM(jet->pt()/1000,jet->eta(),jet->phi(),jet->m()/1000);
      }
      else if (jet->pt()/1000 > jet2.Pt()){
        jet2.SetPtEtaPhiM(jet->pt()/1000,jet->eta(),jet->phi(),jet->m()/1000);
      }
    }
    if(jet1.Pt() != 0 && jet2.Pt() != 0){
      jj = jet1+jet2; 
      h_n_jets.Fill(jets->size());
      h_mjj.Fill(jj.M());
    }
    // counter for the number of events analyzed thus far
    count += 1;
  }
  TFile file("histograms.root","RECREATE");
  h_n_jets.Write();
  h_mjj.Write();
  file.Close();
  

  // exit from the main function cleanly
  return 0;
}

